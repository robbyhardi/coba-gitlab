from app import db
from model.movies import Movies

class Membership(db.Model):
    __tablename__ = 'membership'

    membership_id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column (db.String())
    address = db.Column(db.String())
    salutation = db.Column(db.String())
    movies = db.relationship('Movies')

    def __init__(self, fullname, address, salutation):
        self.fullname = fullname
        self.address = address
        self.salutation = salutation
        
    def __repr__(self):
        return '<membership id {}>'.format(self.membership_id)
        
    def serialize(self):
        return {
            'membership_id':self.membership_id,
            'fullname':self.fullname,
            'address':self.address,
            'salutation':self.salutation
        }