from app import db

class Movies(db.Model):
    __tablename__ = 'movies'

    id = db.Column(db.Integer, primary_key=True)
    membership_id = db.Column(db.Integer, db.ForeignKey('membership.membership_id') )
    movies_rented = db.Column (db.String())
 
    def __init__(self, membership_id, movies_rented):
        self.movies_rented = movies_rented
        self.membership_id = membership_id
        
    def __repr__(self):
        return '<membership id {}>'.format(self.membership_id)
        
    def serialize(self):
        return {
            'id': self.id,
            'membership_id':self.membership_id,
            'movies_rented':self.movies_rented
        }