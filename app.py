from flask import Flask, jsonify, request
# from flask_sqlalchemy import SQLAlchemy

# db = SQLAlchemy()
app = Flask(__name__)

# POSTGRES = {
#     'user'  : 'postgres',
#     'pw'    : 'postgres',
#     'db'    : 'testdb',
#     'host'  : 'localhost',
#     'port'  : '5433'
# }

# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES

# db.init_app(app)
# from model.member import Membership
# from model.movies import Movies

@app.route('/')
def main():
    return 'hello world'

# @app.route('/members', methods=['GET'])
# def get_all_members():
#     try:
#         members = Membership.query.order_by(Membership.membership_id).all()
#         return jsonify({'members': [mbr.serialize() for mbr in members]})
#     except Exception as e:
#         return jsonify(str(e)), 500

# @app.route('/member/<id_>', methods=['GET'])
# def get_member_by_id(id_):
#     try:
#         member = Membership.query.filter_by(membership_id=id_).first()
#         return jsonify(member.serialize())
#     except Exception as e:
#         return jsonify(str(e)), 500

# @app.route('/members', methods=['POST'])
# def add_member():
#     body = request.json
#     try:
#         member = Membership(body['fullname'], body['address'], body['salutation'])
#         db.session.add(member)
#         db.session.commit()
#         return jsonify({
#             'membership_id' : member.membership_id
#         })
#     except Exception as e:
#         return jsonify(str(e)), 500

#     finally:
#         db.session.close()

# @app.route('/member/movies/<id_>', methods=['GET'])
# def get_movies_by_member_id(id_):
#     try:
#         movies = Movies.query.filter_by(membership_id=id_).all()
#         return jsonify({'movies': [mv.serialize() for mv in movies]})
#     except Exception as e:
#         return jsonify(str(e)), 500

# @app.route('/movies', methods=['GET'])
# def get_all_movies():
#     try:
#         movies = Movies.query.order_by(Movies.id).all()
#         return jsonify({'movies': [mv.serialize() for mv in movies]})
#     except Exception as e:
#         return jsonify(str(e)), 500

# @app.route('/member/movies/<id_>', methods=['POST'])
# def add_movies_by_member_id(id_):
#     body = request.json
#     try:
#         for mv in body['movies']:
#             db.session.add(Movies(id_,mv))
#         db.session.commit()
#         return jsonify("Completed!")
#     except Exception as e:
#         return jsonify(str(e)), 500

#     finally:
#         db.session.close()

# @app.route('/movies/<id_>', methods=['DELETE'])
# def del_movie_by_id(id_):
#     try:
#         movies = Movies.query.filter_by(id=id_).first()
#         db.session.delete(movies)
#         db.session.commit()
#         return jsonify("Movie {} has been deleted!".format(id_))
#     except Exception as e:
#         return jsonify(str(e)), 500
#     finally:
#         db.session.close()

# @app.route('/movies/<id_>', methods=['PUT'])
# def update_movie_by_id(id_):
#     try:
#         body = request.json
#         movies = Movies.query.filter_by(id=id_).first()
#         movies.movies_rented = body['movie']
#         db.session.commit()
#         return jsonify("Movie {} has been updated!".format(id_))
#     except Exception as e:
#         return jsonify(str(e)), 500
#     finally:
#         db.session.close()
   

if __name__ == "__main__":
    app.run(port=5000,host='0.0.0.0')